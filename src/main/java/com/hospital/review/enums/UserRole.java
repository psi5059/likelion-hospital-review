package com.hospital.review.enums;

public enum UserRole {
    ADMIN,
    USER
}
