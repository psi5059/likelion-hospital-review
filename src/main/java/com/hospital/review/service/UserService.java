package com.hospital.review.service;

import com.hospital.review.domain.User;
import com.hospital.review.domain.dto.UserJoinRequest;
import com.hospital.review.domain.dto.UserJoinResponse;
import com.hospital.review.exception.AppException;
import com.hospital.review.exception.ErrorCode;
import com.hospital.review.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserService {
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder;

    public UserService(UserRepository userRepository, BCryptPasswordEncoder encoder) {
        this.userRepository = userRepository;
        this.encoder = encoder;
    }

    public UserJoinResponse join(UserJoinRequest userJoinRequest) {
        userRepository.findByUserName(userJoinRequest.getUserName())
                .ifPresent(user->{
                    throw new AppException(ErrorCode.DUPLICATED_USER_NAME, String.format("존재하는 이름입니다."));
                });
        User user=userRepository.save(userJoinRequest.toEntity(encoder.encode(userJoinRequest.getPassword())));
        log.info(user.getUserName());
        return UserJoinResponse.builder()
                .userName(user.getUserName())
                .message("회원가입에 성공했습니다.")
                .build();
    }
}
